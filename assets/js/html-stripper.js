
// http://stackoverflow.com/questions/11890664/how-can-i-strip-certain-html-tags-out-of-a-string


(function () {
	'use strict';


	// TODO: remove attributes toggle
	// TODO: trim toggle
	// [x] TODO: Prism.js syntax coloration
	// TODO: copy button
	// [x] TODO: preview button


	let ALLOWED_TAGS = ["TABLE", "TH", "TR", "TD", "A"];

	const exampleMarkup = document.querySelector ('[data-js="exampleMarkup"');
	const allowedTags = document.querySelector ('[data-js="allowedTags"]');
	const htmlInput = document.querySelector ('[data-js="htmlInput"]');
	const cleanOuput = document.querySelector ('[data-js="cleanOuput"]');
	const buttonClean = document.querySelector ('[data-js="buttonClean"]');
	const buttonPreview = document.querySelector ('[data-js="buttonPreview"]');



	function removeTags (el) {
		let tags = Array.prototype.slice.apply (el.getElementsByTagName("*"), [0]);
		for (let i = 0; i < tags.length; i++) {
			if (ALLOWED_TAGS.indexOf (tags[i].nodeName) == -1) {
				usurp (tags[i]);
			}
		}
	}



	function usurp (p) {
		let last = p;
		for (let i = p.childNodes.length - 1; i >= 0; i--) {
			let e = p.removeChild (p.childNodes[i]);
			if (e.nodeType == 3) continue; // remove text nodes
			p.parentNode.insertBefore (e, last);
			last = e;
		}
		p.parentNode.removeChild (p);
	}



	function clean () {

		ALLOWED_TAGS = !!allowedTags.value ? allowedTags.value.toUpperCase().split (' ') : ALLOWED_TAGS;

		console.info ('cleaning…');
		console.info ('allowed tags: ', ALLOWED_TAGS);

		let div = document.createElement ("div");
		div.innerHTML = htmlInput.value;
		removeTags (div);

		// save raw results to localStorage
		saveState ();

		// clean results
		let results = div.innerHTML;

		// trim spaces
		results = results.trim().replace (/^\s*\n/gm,'');

		// remove comments
		results = results.replace (/<!--[\s\S]*?-->/g,'');

		// display result
		cleanOuput.innerText = results;

		// syntax highlight
		Prism.highlightElement (cleanOuput);

		return div.innerHTML;
	}



	function previewHtml () {
		if (! cleanOuput.innerText) return false;
		let winPreview = window.open ('HTML preview', '_html_preview');
		winPreview.document.write ('<title>HTML preview</title>' + cleanOuput.innerText);
	}



	function saveState () {
		localStorage.setItem ('allowedTags', allowedTags.value);
		localStorage.setItem ('htmlInput', htmlInput.value);
	}



	function loadState () {
		allowedTags.value = localStorage.getItem ('allowedTags');
		htmlInput.value = localStorage.getItem ('htmlInput');
		if (! allowedTags.value) allowedTags.value = 'table thead tbody th tr td a';
	}


	buttonClean.addEventListener ('click', clean);
	buttonPreview.addEventListener ('click', previewHtml);
	window.addEventListener ('load', loadState);

})();
